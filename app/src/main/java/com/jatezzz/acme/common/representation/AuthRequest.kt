package com.jatezzz.acme.common.representation

data class AuthRequest(
    val username: String,
    val password: String
)