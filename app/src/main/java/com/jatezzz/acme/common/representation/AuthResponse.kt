package com.jatezzz.acme.common.representation

data class AuthResponse(
    val userId: Long,
    val isAuthenticated: Boolean
)