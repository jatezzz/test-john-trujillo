package com.jatezzz.acme.common.representation

data class FavoriteProductResponse(
    val id: Long,
    val location: LocationResponse,
    val name: String,
    val price: Double,
    val quantity: Int,
    val total: Double
)