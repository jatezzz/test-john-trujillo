package com.jatezzz.acme.common.representation

data class LocationResponse(
    val latitude: Double,
    val longitude: Double
)