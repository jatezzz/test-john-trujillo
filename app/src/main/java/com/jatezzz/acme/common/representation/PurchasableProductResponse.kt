package com.jatezzz.acme.common.representation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PurchasableProductResponse(
    val id: Long,
    val name: String,
    val stock: Int,
    val unitPrice: Double
) : Parcelable