package com.jatezzz.acme.common.representation

data class UserFavoriteProductRequest(
    val id: Int,
    val location: LocationResponse,
    val price: Double,
    val quantity: Int,
    val total: Double
)