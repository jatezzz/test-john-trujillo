package com.jatezzz.acme.common.representation

data class UserResponse(
    var favoriteProducts: List<FavoriteProductResponse>,
    val id: Int,
    val name: String
)