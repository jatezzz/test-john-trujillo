package com.jatezzz.acme.common.service

import com.jatezzz.acme.common.representation.AuthResponse
import com.jatezzz.acme.common.representation.PurchasableProductResponse
import com.jatezzz.acme.common.representation.UserResponse
import com.jatezzz.acme.common.retrofit.ResultType
import retrofit2.http.GET
import retrofit2.http.Path

const val AUTH_USER = "auth"
const val FETCH_USER_INFO = "users/{id}"
const val FETCH_PRODUCTS = "products"
const val UPDATE_USER_FAVORITES = "users/2"

interface ExternalService {

    @GET(AUTH_USER)
    suspend fun authUser(): ResultType<AuthResponse>

    @GET(FETCH_USER_INFO)
    suspend fun fetchUserInfo(@Path("id") id: Long = 0): ResultType<UserResponse>

    @GET(FETCH_PRODUCTS)
    suspend fun fetchAllProducts(): ResultType<List<PurchasableProductResponse>>

    @GET(UPDATE_USER_FAVORITES)
    suspend fun updateUserFavorites(): ResultType<UserResponse>

}
