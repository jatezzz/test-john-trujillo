package com.jatezzz.acme.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products")
class Product(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val name: String = "",
    val price: Double = 0.0,
    val quantity: Int = 0,
    val total: Double = 0.0
)
