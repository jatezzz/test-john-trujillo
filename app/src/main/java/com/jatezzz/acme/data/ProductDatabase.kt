package com.jatezzz.acme.data

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * The Room Database that contains the Scenes table.
 *
 * Note that exportSchema should be true in production databases.
 */
@Database(
  entities = [Product::class],
  version = 1,
  exportSchema = false
)

abstract class ProductDatabase : RoomDatabase() {

  abstract fun sceneDao(): ProductDao

  suspend fun getProducts(): List<Product> {
    return sceneDao().getProducts()
  }

  suspend fun insertProducts(products: List<Product>) {
    sceneDao().insertMultipleRecord(products)
  }
}
