package com.jatezzz.acme.data

import com.jatezzz.acme.common.representation.FavoriteProductResponse
import com.jatezzz.acme.common.representation.LocationResponse
import com.jatezzz.acme.common.retrofit.ResultType
import com.jatezzz.acme.common.retrofit.succeeded
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val productsLocalDataSource: ProductsLocalDataSource,
    private val productsRemoteDataSource: RemoteDataSource
) {

    suspend fun retrieveProductList(
        fromLocal: Boolean,
        userId: Long = 0
    ): ResultType<List<FavoriteProductResponse>> {
        if (fromLocal) {
            val fetchProductList = productsLocalDataSource.fetchProductList()
            if (fetchProductList.succeeded) {
                val mappedData = (fetchProductList as ResultType.Success).data.map {
                    FavoriteProductResponse(
                        it.id,
                        LocationResponse(it.latitude, it.longitude),
                        it.name,
                        it.price,
                        it.quantity,
                        it.total
                    )
                }
                return ResultType.Success(mappedData)
            }
            return fetchProductList as ResultType.Error
        } else {
            val fetchProductList = productsRemoteDataSource.fetchUserInfo()
            if (fetchProductList.succeeded) {
                val mappedData = (fetchProductList as ResultType.Success).data.favoriteProducts
                return ResultType.Success(mappedData)
            }
            return fetchProductList as ResultType.Error
        }
    }

    suspend fun saveProducts(products: List<FavoriteProductResponse>) {
        productsLocalDataSource.saveProduct(products.map {
            Product(
                it.id,
                it.location.latitude,
                it.location.longitude,
                it.name,
                it.price,
                it.quantity,
                it.total
            )
        })
    }

}