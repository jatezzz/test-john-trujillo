package com.jatezzz.acme.data

import com.jatezzz.acme.common.retrofit.ResultType
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProductsLocalDataSource internal constructor(
    private val sceneDatabase: ProductDatabase,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun fetchProductList() = withContext(ioDispatcher) {
        try {
            val sceneList = sceneDatabase.getProducts()
            return@withContext ResultType.Success(sceneList)
        } catch (e: Exception) {
            return@withContext ResultType.Error(e)
        }
    }

    suspend fun saveProduct(products: List<Product>) =
        withContext(ioDispatcher) {
            return@withContext sceneDatabase.insertProducts(products)
        }

}
