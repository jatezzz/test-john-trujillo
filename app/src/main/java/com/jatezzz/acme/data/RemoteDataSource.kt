package com.jatezzz.acme.data

import com.jatezzz.acme.common.representation.AuthRequest
import com.jatezzz.acme.common.representation.AuthResponse
import com.jatezzz.acme.common.representation.PurchasableProductResponse
import com.jatezzz.acme.common.representation.UserResponse
import com.jatezzz.acme.common.retrofit.ResultType
import com.jatezzz.acme.common.service.ExternalService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

var USER_ID: Long = 1
class RemoteDataSource internal constructor(
    private val externalService: ExternalService,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun authUser(request: AuthRequest): ResultType<AuthResponse> =
        withContext(ioDispatcher) {
            // Request is ignored due to configuration of the mock server
            return@withContext externalService.authUser()
        }

    suspend fun fetchUserInfo(id: Long = USER_ID): ResultType<UserResponse> =
        withContext(ioDispatcher) {
            return@withContext externalService.fetchUserInfo(id)
        }

    suspend fun fetchAllProducts(): ResultType<List<PurchasableProductResponse>> =
        withContext(ioDispatcher) {
            return@withContext externalService.fetchAllProducts()
        }

    suspend fun updateUserFavorites(
        request: UserResponse
    ): ResultType<UserResponse> = withContext(ioDispatcher) {
        // Request is ignored due to configuration of the mock server
        USER_ID = 2
        return@withContext externalService.updateUserFavorites()
    }

}
