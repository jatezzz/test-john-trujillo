package com.jatezzz.acme.di

import android.content.Context
import androidx.room.Room
import com.jatezzz.acme.common.service.ExternalService
import com.jatezzz.acme.data.ProductDatabase
import com.jatezzz.acme.data.ProductRepository
import com.jatezzz.acme.data.ProductsLocalDataSource
import com.jatezzz.acme.data.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesProductRepository(
        scenesLocalDataSource: ProductsLocalDataSource,
        scenesRemoteDataSource: RemoteDataSource
    ): ProductRepository {
        return ProductRepository(scenesLocalDataSource, scenesRemoteDataSource)
    }

    @Provides
    @Singleton
    fun providesRemoteDataSource(
        externalService: ExternalService,
        ioDispatcher: CoroutineDispatcher
    ): RemoteDataSource {
        return RemoteDataSource(externalService, ioDispatcher)
    }

    @Provides
    @Singleton
    fun providesProductsLocalDataSource(
        database: ProductDatabase,
        ioDispatcher: CoroutineDispatcher
    ): ProductsLocalDataSource {
        return ProductsLocalDataSource(database, ioDispatcher)
    }

    @Provides
    @Singleton
    fun provideDataBase(@ApplicationContext context: Context): ProductDatabase {
        return Room.databaseBuilder(
            context,
            ProductDatabase::class.java,
            "ProductDatabase.db"
        )
            .build()
    }

    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO
}