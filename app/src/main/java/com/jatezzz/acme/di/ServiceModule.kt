package com.jatezzz.acme.di

import com.jatezzz.acme.common.service.ExternalService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class ServiceModule {

  @Provides
  @Singleton
  fun provideExternalService(retrofit: Retrofit): ExternalService {
      return retrofit.create(ExternalService::class.java)
  }
}
