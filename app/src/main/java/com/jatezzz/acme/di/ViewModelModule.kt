package com.jatezzz.acme.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jatezzz.acme.base.BaseViewModelFactory
import com.jatezzz.acme.list.ListViewModel
import com.jatezzz.acme.main.MainViewModel
import com.jatezzz.acme.marketplace.MarketPlaceViewModel
import com.jatezzz.acme.purchasedetail.PurchaseViewModel
import com.jatezzz.acme.ui.login.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoMap

@InstallIn(SingletonComponent::class)
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: BaseViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun mainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun loginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    internal abstract fun listViewModel(viewModel: ListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketPlaceViewModel::class)
    internal abstract fun marketPlaceViewModel(viewModel: MarketPlaceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PurchaseViewModel::class)
    internal abstract fun purchaseViewModel(viewModel: PurchaseViewModel): ViewModel

}