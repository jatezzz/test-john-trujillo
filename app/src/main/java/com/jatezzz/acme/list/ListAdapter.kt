package com.jatezzz.acme.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jatezzz.acme.R
import com.jatezzz.acme.common.representation.FavoriteProductResponse

class ListAdapter(
    val onClickAction: (FavoriteProductResponse) -> Unit,
    private var datalist: ArrayList<FavoriteProductResponse> = ArrayList()
) :
    RecyclerView.Adapter<ListAdapter.ShowViewHolder>() {

    override fun getItemCount(): Int = datalist.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_show, parent, false)
        val holder = ShowViewHolder(view)
        holder.itemView.setOnClickListener {
            onClickAction(datalist[holder.adapterPosition])
        }
        return holder
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        val data = datalist[position]
        holder.name.text = data.name
        holder.price.text = data.price.toString()
        holder.quantity.text = data.quantity.toString()
        holder.total.text = data.total.toString()
        holder.location.text = "${data.location.latitude} - ${data.location.longitude}"
    }

    fun setData(data: List<FavoriteProductResponse>) {
        val lastPosition = datalist.size
        datalist = ArrayList()
        notifyItemRangeRemoved(0, lastPosition)
        datalist.addAll(data)
        notifyItemRangeInserted(0, datalist.size)
    }

    class ShowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val price: TextView = itemView.findViewById(R.id.price)
        val quantity: TextView = itemView.findViewById(R.id.quantity)
        val total: TextView = itemView.findViewById(R.id.total)
        val location: TextView = itemView.findViewById(R.id.location)
    }

}
