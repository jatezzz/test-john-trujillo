package com.jatezzz.acme.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.jatezzz.acme.R
import com.jatezzz.acme.databinding.FragmentListBinding
import com.jatezzz.acme.ui.login.LoginActivity
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class ListFragment : Fragment(R.layout.fragment_list) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var model: ListViewModel

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    private lateinit var listAdapter: ListAdapter

    private val loadingObserver = Observer<Boolean> {

    }

    private val listObserver = Observer<ListViewModel.ViewData> { incomingData ->
        listAdapter.setData(incomingData.shows)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentListBinding.bind(view)
        binding.lifecycleOwner = this

        model = ViewModelProvider(this, viewModelFactory)[ListViewModel::class.java]

        model.isLoading.observe(viewLifecycleOwner, loadingObserver)
        model.incomingShowList.observe(viewLifecycleOwner, listObserver)

        listAdapter = ListAdapter({

        })
        binding.recyclerview.adapter = listAdapter

        binding.buttonAddProduct.setOnClickListener {
            val action =
                ListFragmentDirections.actionListFragmentToMarketPlaceFragment()
            try {
                findNavController().navigate(action)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }

        binding.buttonLogout.setOnClickListener {
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            activity?.finish()
        }
    }


    override fun onStart() {
        super.onStart()
        model.loadData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
