package com.jatezzz.acme.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jatezzz.acme.common.representation.FavoriteProductResponse
import com.jatezzz.acme.common.retrofit.ResultType
import com.jatezzz.acme.data.ProductRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject


class ListViewModel @Inject constructor(private val productRepository: ProductRepository) :
    ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading


    private val _incomingShowList = MutableLiveData<ViewData>()
    val incomingShowList: LiveData<ViewData> = _incomingShowList

    fun loadData() {
        _isLoading.value = true
        viewModelScope.launch {
            var userProducts = retrieveLocalList()
            _incomingShowList.value = ViewData(userProducts, true)
            delay(2000)
            userProducts = retrieveRemoteList()
            _incomingShowList.value = ViewData(userProducts, true)
            saveProducts(userProducts)
            _isLoading.value = false
        }
    }

    private suspend fun retrieveLocalList(): List<FavoriteProductResponse> {
        val rawResult = productRepository.retrieveProductList(true)
        return if (rawResult is ResultType.Success) {
            rawResult.data
        } else {
            arrayListOf()
        }
    }

    private suspend fun retrieveRemoteList(): List<FavoriteProductResponse> {
        val rawResult = productRepository.retrieveProductList(false, 1)
        return if (rawResult is ResultType.Success) {
            rawResult.data
        } else {
            arrayListOf()
        }
    }

    private suspend fun saveProducts(userProducts: List<FavoriteProductResponse>) {
        productRepository.saveProducts(userProducts)
    }

    data class ViewData(
        val shows: List<FavoriteProductResponse>,
        val hasAppendableData: Boolean
    )
}