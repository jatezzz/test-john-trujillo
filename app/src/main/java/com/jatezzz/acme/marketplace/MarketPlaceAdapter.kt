package com.jatezzz.acme.marketplace

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jatezzz.acme.R
import com.jatezzz.acme.common.representation.PurchasableProductResponse

class MarketPlaceAdapter(
    val onClickAction: (PurchasableProductResponse) -> Unit,
    private var datalist: ArrayList<PurchasableProductResponse> = ArrayList()
) :
    RecyclerView.Adapter<MarketPlaceAdapter.ShowViewHolder>() {

    override fun getItemCount(): Int = datalist.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_purchasable_product, parent, false)
        val holder = ShowViewHolder(view)
        holder.itemView.setOnClickListener {
            onClickAction(datalist[holder.adapterPosition])
        }
        return holder
    }

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        val data = datalist[position]
        holder.name.text = data.name
    }

    fun setData(data: List<PurchasableProductResponse>) {
        val lastPosition = datalist.size
        datalist = ArrayList()
        notifyItemRangeRemoved(0, lastPosition)
        datalist.addAll(data)
        notifyItemRangeInserted(0, datalist.size)
    }

    class ShowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
    }

}
