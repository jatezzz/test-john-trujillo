package com.jatezzz.acme.marketplace

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.jatezzz.acme.R
import com.jatezzz.acme.common.representation.PurchasableProductResponse
import com.jatezzz.acme.databinding.FragmentMarketplaceBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class MarketPlaceFragment : Fragment(R.layout.fragment_marketplace) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var model: MarketPlaceViewModel

    private var _binding: FragmentMarketplaceBinding? = null
    private val binding get() = _binding!!

    private lateinit var listAdapter: MarketPlaceAdapter

    private val loadingObserver = Observer<Boolean> {

    }

    private val listObserver = Observer<List<PurchasableProductResponse>> { incomingData ->
        listAdapter.setData(incomingData)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentMarketplaceBinding.bind(view)
        binding.lifecycleOwner = this

        model = ViewModelProvider(this, viewModelFactory)[MarketPlaceViewModel::class.java]

        model.isLoading.observe(viewLifecycleOwner, loadingObserver)
        model.incomingShowList.observe(viewLifecycleOwner, listObserver)

        listAdapter = MarketPlaceAdapter({
            val action =
                MarketPlaceFragmentDirections.actionMarketPlaceFragmentToPurchaseFragment(it)
            try {
                findNavController().navigate(action)
            } catch (e: Exception) {
                Timber.e(e)
            }
        })
        binding.recyclerview.adapter = listAdapter

    }


    override fun onStart() {
        super.onStart()
        model.loadProducts()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
