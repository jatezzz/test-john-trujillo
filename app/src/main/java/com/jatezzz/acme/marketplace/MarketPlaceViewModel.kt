package com.jatezzz.acme.marketplace

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jatezzz.acme.common.representation.PurchasableProductResponse
import com.jatezzz.acme.common.retrofit.ResultType
import com.jatezzz.acme.data.RemoteDataSource
import kotlinx.coroutines.launch
import javax.inject.Inject


class MarketPlaceViewModel @Inject constructor(private val remoteDataSource: RemoteDataSource) :
    ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading


    private val _incomingShowList = MutableLiveData<List<PurchasableProductResponse>>()
    val incomingShowList: LiveData<List<PurchasableProductResponse>> = _incomingShowList

    fun loadProducts() {
        _isLoading.value = true
        viewModelScope.launch {
            fetchAllProducts()
            _isLoading.value = false
        }
    }

    private suspend fun fetchAllProducts() {
        val rawResult = remoteDataSource.fetchAllProducts()
        val products = if (rawResult is ResultType.Success) {
            rawResult.data
        } else {
            arrayListOf()
        }
        _incomingShowList.value = products
    }

}