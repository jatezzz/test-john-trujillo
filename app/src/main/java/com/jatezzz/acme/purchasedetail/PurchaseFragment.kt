package com.jatezzz.acme.purchasedetail

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.jatezzz.acme.R
import com.jatezzz.acme.databinding.FragmentPurchaseBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class PurchaseFragment : Fragment(R.layout.fragment_purchase) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var model: PurchaseViewModel

    private var _binding: FragmentPurchaseBinding? = null
    private val binding get() = _binding!!

    private val loadingObserver = Observer<Boolean> {

    }
    private val isStatusObserver = Observer<Boolean> {
        if (it) {
            val action =
                PurchaseFragmentDirections.actionPurchaseFragmentToListFragment()
            try {
                findNavController().navigate(action)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private val listObserver = Observer<PurchaseViewModel.ViewData> { data ->
        binding.name.text = data.data.name
        binding.stock.text = data.data.stock.toString()
        binding.unitPrice.text = "\$${data.data.unitPrice}"
        binding.total.text = "\$${data.total}"
        binding.buyButton.isEnabled = data.purchaseAvailable
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = FragmentPurchaseBinding.bind(view)
        binding.lifecycleOwner = this

        model = ViewModelProvider(this, viewModelFactory)[PurchaseViewModel::class.java]

        model.isLoading.observe(viewLifecycleOwner, loadingObserver)
        model.incomingShowList.observe(viewLifecycleOwner, listObserver)
        model.isStatus.observe(viewLifecycleOwner, isStatusObserver)
        val args: PurchaseFragmentArgs by navArgs()
        model.saveArgs(args.product)

        binding.quantity.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    model.updateTotal(s.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Unused but required for object inheritance
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //Unused but required for object inheritance
            }
        }
        )
        binding.buyButton.setOnClickListener {
            model.buy()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
