package com.jatezzz.acme.purchasedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jatezzz.acme.common.representation.FavoriteProductResponse
import com.jatezzz.acme.common.representation.LocationResponse
import com.jatezzz.acme.common.representation.PurchasableProductResponse
import com.jatezzz.acme.common.retrofit.ResultType
import com.jatezzz.acme.data.RemoteDataSource
import kotlinx.coroutines.launch
import javax.inject.Inject


class PurchaseViewModel @Inject constructor(private val remoteDataSource: RemoteDataSource) :
    ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading


    private val _incomingShowList = MutableLiveData<ViewData>()
    val incomingShowList: LiveData<ViewData> = _incomingShowList

    private val _isStatus = MutableLiveData<Boolean>()
    val isStatus: LiveData<Boolean> = _isStatus

    fun saveArgs(product: PurchasableProductResponse?) {
        product?.let {
            _incomingShowList.value = ViewData(it)
        }
    }

    fun updateTotal(quantity: String) {
        val newQuantity = try {
            Integer.valueOf(quantity)
        } catch (e: Exception) {
            0
        }
        _incomingShowList.value?.data?.let {
            if (newQuantity <= it.stock) {
                val total = newQuantity * it.unitPrice
                _incomingShowList.value = ViewData(it, total, total != 0.0, newQuantity)
            } else {
                _incomingShowList.value = ViewData(it, 0.0, false, 0)
            }
        }
    }

    fun buy() {
        _isLoading.value = true
        viewModelScope.launch {
            purchaseProduct()
            _isLoading.value = false
        }
    }

    private suspend fun purchaseProduct() {
        _incomingShowList.value?.let { originalData ->

            val userInfo = remoteDataSource.fetchUserInfo()
            if (userInfo is ResultType.Success) {
                val newInfo = userInfo.data
                val list: ArrayList<FavoriteProductResponse> = arrayListOf()
                list.addAll(newInfo.favoriteProducts)
                list.add(
                    FavoriteProductResponse(
                        originalData.data.id,
                        LocationResponse(0.0, 0.0),
                        originalData.data.name,
                        originalData.data.unitPrice,
                        originalData.quantity,
                        originalData.total
                    )
                )
                newInfo.favoriteProducts = list

                val rawResult = remoteDataSource.updateUserFavorites(newInfo)
                if (rawResult is ResultType.Success) {
                    _isStatus.value = true
                }
            }
        }

    }

    data class ViewData(
        val data: PurchasableProductResponse,
        val total: Double = 0.0,
        val purchaseAvailable: Boolean = false,
        val quantity: Int = 0
    )

}